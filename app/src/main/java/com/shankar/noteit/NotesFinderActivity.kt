package com.shankar.noteit

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_notes_finder.*

class NotesFinderActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener  {

    var isFragmentNotesLoaded = true
    val manager = supportFragmentManager
    val transaction = manager.beginTransaction()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_finder)

        setupNavigationMenu()

        showFragmentNotes()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
    fun setupNavigationMenu() {
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, /* host activity */
                drawer_layout, /* Drawer Layout object */
                toolbar, /* Toolbar Object */
                //TODO:Change these two
                R.string.app_name, /* description for accessibility */
                R.string.app_name  /* description for accessibility */
        )
        // Set the drawer toggle as the DrawerListener
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        // Set-up the NavigationView and its listener
        nav_view.setNavigationItemSelectedListener(this)
    }

    fun showFragmentNotes() {

        val notesFragment = NotesFragment()
        transaction.replace(R.id.notesListFragment, notesFragment)
        transaction.addToBackStack(null)
        transaction.commit()
        isFragmentNotesLoaded = false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.navHomeMenuItem -> Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
            R.id.navNotesMenuItem -> Toast.makeText(this, "Images", Toast.LENGTH_SHORT).show()
            R.id.navSettingsMenuItem -> Toast.makeText(this, "Videos", Toast.LENGTH_SHORT).show()
            R.id.navAboutsMenuItem -> Toast.makeText(this, "Tools", Toast.LENGTH_SHORT).show()
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
